# CORRECTIONS MADE:
#   1. return statement appears only once in function
#   2. if argument is ont of list type then it prints: "arg not of <list> type"
#   3. correction in print output


def isListOfInts(lst):
    flag = False
    if len(lst) == 0:
        flag = True
    elif len(lst) > 0:
        for i in lst:
            m = str(type(i).__name__)
            if m == "int":
                flag = True
            else:
                flag = False
                break
    return flag

#initial list to be input by user

try:
    lst = [1,3.0]
    if str(type(lst).__name__) == "list":
        print lst, " --> ", isListOfInts(lst)
    else:
        print ("arg not of <list> type")
except:
    print ("arg not of <list> type")